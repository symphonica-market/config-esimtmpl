<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# Hello World

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)
* [Connector Resource Orders](#connector-resource-orders)

## Overview

This Quick-Start-Pack demonstrates a simple workflow with an HTTP connector and integration to the Service Inventory using the eSIM (Embedded Subscriber Identity Module) provisioning process as example.

The package includes the eSIM **Download preparation process** use case using the **ES2+ interface** as defined by GSMA in [SGP.22-2.4](https://www.gsma.com/esim/wp-content/uploads/2021/10/SGP.22-2.4.pdf). A simulator of the Subscription Manager Data Preparation platform (SM-DP+) is also provided.

<table><tr><td>
  <img src="./doc/images/overview.jpg" alt="workflow" width="800px">
</td></tr></table> 

<!-- Write a few sentences about the Pre-Built and explain the use case(s) -->
<!-- Avoid using the word Artifact. Please use Pre-Built, Pre-Built Transformation or Pre-Built Automation -->
<!-- Ex.: The Migration Wizard enables IAP users to conveniently move their automation use cases between different IAP environments -->
<!-- (e.g. from Dev to Pre-Production or from Lab to Production). -->

<!-- Workflow(s) Image Placeholder - TO BE ADDED DIRECTLY TO GitLab -->
<!-- REPLACE COMMENT BELOW WITH IMAGE OF YOUR MAIN WORKFLOW -->
<!--

-->
<!-- REPLACE COMMENT ABOVE WITH IMAGE OF YOUR MAIN WORKFLOW -->

<!-- ADD ESTIMATED RUN TIME HERE -->
<!-- e.g. Estimated Run Time: 34 min. -->

_Estimated Run Time_: 10 minutes

## Requirements

This Quick-Start-Pack (QSP) requires the following:

<!-- Unordered list highlighting the requirements of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * cisco ios device -->
<!-- * Ansible or NSO (with F5 NED) * -->
* Symphonica Platform
  * `^2.6.0`
* Tenant created.
* User with *Administrator* role.
* [Postman](https://www.postman.com/) installed on user machine and [collections](https://www.getpostman.com/collections/263a2abc8d5cedf18abb) loaded (optonal for REST API example).
* [Symphonica Basic Knowledge](https://symphonica.com/docs/get-started/)

## Features

The main benefits and features of the Quick-Start-Pack are outlined below.

Every Quick-Start-Pack is designed to optimize network performance and configuration by focusing on reliability, flexibility and coordination of network changes with applications and IT processes.

<!-- Unordered list highlighting the most exciting features of the Pre-Built -->
<!-- EXAMPLE -->
<!-- * Automatically checks for device type -->
<!-- * Displays dry-run to user (asking for confirmation) prior to pushing config to the device -->
<!-- * Verifies downloaded file integrity (using md5), will try to download again if failed -->


## How to Install

* Verify the [Requirements](#requirements) section in order to install the Quick-Start-Pack.
* Quick-Start-Packs can be installed from the [Marketplace](https://qa.cloud.intraway.com/#/home/marketplace) section in the Account Manager console. Simply search for the name of your desired Quick-Start-Pack and click the install button (as shown below).

**1. Install the *Base Package* Quick-Start-Pack (*skip* if already installed):**

[![](http://img.youtube.com/vi/B-8sV7VRylE/0.jpg)](https://youtu.be/B-8sV7VRylE "eSIM Hello World quick launch package base package installation")

**2. Install the *eSIM Hello World* Quick-Start-Pack:**

[![](http://img.youtube.com/vi/H9wW1vhpX0g/0.jpg)](https://youtu.be/H9wW1vhpX0g "eSIM Hello World quick launch package installation")

**3. Add the *Subscription Manager Data Preparation platform (SM-DP+) NMS* to the Connector Cluster:**

Launch [Symphonica Console](https://qa.cloud.intraway.com/console/) and follow video:

[![](http://img.youtube.com/vi/QmT6OvxHMIE/0.jpg)](https://youtu.be/QmT6OvxHMIE "eSIM Hello World quick launch package add NMS to Connector Cluster")

**4. Test the *eSIM Hello World* Quick-Start-Pack:**

Using Symphonicas's *Resource Order Test* functionality, it is possible to run *Resource Orders* to test the Quick-Start-Pack, validate the installation see how it works.

Access [Resource Order Test](https://qa.cloud.intraway.com/console/#/resource-order-test/find) on Symphonica Console and follow video:

[![](http://img.youtube.com/vi/Rz_VSDYoQmA/0.jpg)](https://youtu.be/Rz_VSDYoQmA "eSIM Hello World quick launch package basic package test guide")

> That's it you are all set to use the *eSIM Hello World Quick-Start-Pack*.

<!-- REPLACE BELOW WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->
<!--
<table><tr><td>
  <img src="./images/install.png" alt="install" width="600px">
</td></tr></table>
-->
<!-- REPLACE ABOVE WITH IMAGE OF YOUR PUBLISHED PRE-BUILT -->

<!-- OPTIONAL - Explain if external components are required outside of IAP -->
<!-- Ex.: The Ansible roles required for this Pre-Built can be found in the repository located at https://gitlab.com/itentialopensource/pre-built-automations/hello-world -->

## How to Run

The required initial values to be passed on to the Quick-Start-Pack are:
- None

Use the following to run the Quick-Start-Pack use cases using [Symphonica Console](https://qa.cloud.intraway.com/console/):

**1. Create new eSIM with auto-assign ICCID:**

The workflow will execute the *Download Preparation Process* as described by [GSMA](https://www.gsma.com/esim/wp-content/uploads/2021/10/SGP.22-2.4.pdf) using the *ES2+.DownloadOrder* interface. This will create the new eSIM in *SM-DP+* and return the assigned *ICCID (Integrated Circuit Card Identification Number)* that will be stored on Symphonica's inventory.

[![](http://img.youtube.com/vi/fp7Iro6H0F4/0.jpg)](https://youtu.be/fp7Iro6H0F4 "eSIM Hello World quick launch package use case execution SymWeb empty iccid")

**2. Remove eSIM:**

The workflow will remove the eSIM from *SM-DP+* using the *ES2+.CancelOrder* interface and remove the eSIM from Symphonica's inventory.

[![](http://img.youtube.com/vi/4t-A9rxwUu4/0.jpg)](https://youtu.be/4t-A9rxwUu4 "eSIM Hello World quick launch package use case execution SymWeb Remove")

## Connector Resource Orders

**RSP.DownloadOrder**

| Input variable  | Description |
| ------------- |:-------------|
| iccid      | Integrated Circuit Card Identifier    |
| eid      | Identification of the targeted eUICC     |
| profileType      | Identification of the Profile Type to download and install in the eUICC     |

| Output variable  | Description |
| ------------- |:-------------|
| iccid      | Returns the iccid entered in the input value, for the case that no value is entered in the input, the SM-DP+ does the assignment    |
| transactionId      | Transaction ID generated by SM-DP+     |
| orderStatus      | Order Status, returned by SM-DP+  |